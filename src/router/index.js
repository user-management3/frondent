import { createWebHistory, createRouter } from "vue-router";
import Login from "../views/LoginView.vue";
import Register from "../views/RegisterView.vue";
import EditProfile from "../views/EditProfil.vue";
import Activity from "../views/Activity.vue";
import Home from "../views/HomeView.vue";
const Profile = () =>
    import ("../views/ProfileView.vue");
const BoardAdmin = () =>
    import ("../views/BoardAdmin.vue");
const routes = [{
        path: "/",
        name: "login",
        component: Login,
    },
    {
        path: "/home",
        name: "home",
        component: Home,
    },
    {
        path: "/login",
        component: Login,
    },
    {
        path: "/register",
        component: Register,
    },
    {
        path: "/profile",
        name: "profile",
        component: Profile,
    },
    {
        path: "/actives",
        name: "actives",
        component: Activity,
    },
    {
        path: "/profil/:id",
        name: "edit-profil",
        component: () =>
            import ("../views/EditProfil"),
    },
    {
        path: "/edit",
        name: "edit-profile",
        component: EditProfile,
    },

    {
        path: "/admin",
        name: "admin",
        component: BoardAdmin,
    },
    {
        path: "/roles",
        alias: "/roles",
        name: "roles",
        component: () =>
            import ("../views/RolesList"),
    },

    {
        path: "/users",
        alias: "/users",
        name: "users",
        component: () =>
            import ("../views/UsersList"),
    },
    {
        path: "/users/:id",
        name: "user-details",
        component: () =>
            import ("../views/UserDetails"),
    },

    {
        path: "/profile/:id",
        name: "profile-view",
        component: () =>
            import ("../views/EditProfil"),
    },
];
const router = createRouter({
    history: createWebHistory(),
    routes,
});
router.beforeEach((to, from, next) => {
    const publicPages = ["/login", "/register", "/home"];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem("user");
    if (authRequired && !loggedIn) {
        next("/login");
    } else {
        next();
    }
});
export default router;