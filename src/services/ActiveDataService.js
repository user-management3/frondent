import http from "../http-common";

class ActiveDataService {
    getAll() {
        return http.get("/actives");
    }
}
export default new ActiveDataService();