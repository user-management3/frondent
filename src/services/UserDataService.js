import http from "../http-common";

class UserDataService {
  getAll(params) {
    return http.get("/users", { params });
  }
  get(id) {
    return http.get(`/users/${id}`);
  }
  create(data) {
    return http.post("/actives", data);
  }
  update(id, data) {
    return http.put(`/users/${id}`, data);
  }
  delete(id) {
    return http.delete(`/users/${id}`);
  }
  findById(id) {
    return http.get(`/users?id=${id}`);
  }
}
export default new UserDataService();
